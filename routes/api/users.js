const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../../models/User');
const config = require('config');
// const { check, validationResult} = require('express-validator/check');

// @route   POST api/users
// @desc    Register route
// @access  Public
router.post(
    '/',
    [
        check('firstName', 'First name is required')
            .not()
            .isEmpty(),
        check('lastName', 'Last name is required')
            .not()
            .isEmpty(),
        check('status', 'Status is required')
            .not()
            .isEmpty(),
        check('roleNumber', 'Role number is required')
            .not()
            .isEmpty(),
        check('email', 'Please include a valid email').isEmail(),
        check(
            'password',
            'Please enter a password with 8 or more characters'
        ).isLength({ min: 8 })
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { firstName, lastName, email, password, status, roleNumber } = req.body;

        try {
            let user = await User.findOne({ email });

            if (user) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: 'User already exists' }] });
            }

            //check if role number already exists
            user = await User.findOne({ roleNumber });

            if (user) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: 'Role number already exists' }] });
            }

            user = new User({
                firstName,
                lastName,
                email,
                password,
                status,
                roleNumber
            });

            const salt = await bcrypt.genSalt(10);

            user.password = await bcrypt.hash(password, salt);

            await user.save();

            const payload = {
                user: {
                    id: user.id
                }
            };

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    res.json({ token });
                }
            );
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
);

module.exports = router;
