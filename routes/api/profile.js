const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Profile = require('../../models/Profile');
const User = require('../../models/Profile');

// @route   GET api/profile/me
// @desc    Get Current user's progile
// @access  Private
router.get('/me', auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({ user: req.user.id }).populate(
            'user',
            ['name']
        );

        if (!profile) {
            return res
                .status(400)
                .json({ msg: 'There is no profile for this user' });
        }

        res.json(profile);
    } catch (err) {
        console.error(err);
        res.status(500).send('Server Error');
    }
});

// @route   POST api/profile/
// @desc    Create or update user profile
// @access  Private
router.post(
    '/',
    auth,
    async (req, res) => {
        // const errors = validationResult(req);
        // if (!errors.isEmpty()) {
        //     return res.status(400).json({ errors: errors.array() });
        // }

        const { phoneNumber, personalEmail, major, minor } = req.body;

        const profileFields = {};
        profileFields.user = req.user.id;
        if (phoneNumber) profileFields.phoneNumber = phoneNumber;
        if (personalEmail) profileFields.personalEmail = personalEmail;
        if (major) profileFields.major = major;
        if (minor) profileFields.minor = minor;

        try {
            let profile = await Profile.findOne({ user: req.user.id });

            if (profile) {
                // Update
                profile = await Profile.findOneAndUpdate(
                    { user: req.user.id },
                    { $set: profileFields },
                    { new: true }
                );
                return res.json(profile);
            }

            // Create
            profile = new Profile(profileFields);

            await profile.save();
            return res.json(profile);
        } catch (err) {
            console.error(err);
            return res.status(500).send('Server Error');
        }
    }
);

module.exports = router;
