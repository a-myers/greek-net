import React, { Fragment, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Alert from './components/layout/Alert';
import Dashboard from './components/dashboard/Dashboard';
import PrivateRoute from './components/routing/PrivateRoute';
import Profile from './components/dashboard/Profile';

import { makeStyles } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/core/styles';

//Redux
import { Provider } from 'react-redux';
import store from './store';
import setAuthToken from './utils/setAuthToken';
import { loadUser } from './actions/auth';
import { mergeClasses } from '@material-ui/styles';

if (localStorage.token) {
    setAuthToken(localStorage.token);
}

const useStyles = makeStyles(theme => ({
    authContent: {
        flexGrow: 1,
        padding: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            marginLeft: 240
        }
    }
}));

const App = () => {
    useEffect(() => {
        store.dispatch(loadUser());
    }, []);

    const classes = useStyles();
    const theme = useTheme();

    return (
        <Provider store={store}>
            <Router>
                <Fragment>
                    <CssBaseline />
                    <Navbar />
                    {/* <div className={classes.content}> */}
                    <Alert />
                    <Switch>
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/login" component={Login} />
                        <div className={classes.authContent}>
                            <PrivateRoute
                                exact
                                path="/dashboard"
                                component={Dashboard}
                            />
                            <PrivateRoute
                                exact
                                path="/profile"
                                component={Profile}
                            />
                        </div>
                        <Route path="/" component={Landing} />
                    </Switch>
                    {/* </div> */}
                </Fragment>
            </Router>
        </Provider>
    );
};

export default App;
