import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentProfile } from '../../actions/profile';
import {
    Container,
    Grid,
    Paper,
    Typography,
    Divider,
    Hidden,
    Button,
    Link
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    card: {
        // padding: theme.spacing(1, 1, 1, 1),
        // paddingBottom: '5px !important'
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    header: {
        paddingTop: 0,
        marginBottom: 0,
        marginTop: 0,
        [theme.breakpoints.up('sm')]: {
            textAlign: 'left',
            align: 'left'
        },
        [theme.breakpoints.down('xs')]: {
            textAlign: 'center',
            align: 'center !important'
        }
    },
    // headerEdit: {
    //     margin: 0
    // },
    title: {
        textAlign: 'center'
    },
    titleGrid: {
        marginBottom: theme.spacing(2)
    },
    titleLeft: {
        [theme.breakpoints.up('sm')]: {
            textAlign: 'left'
        },
        [theme.breakpoints.down('xs')]: {
            textAlign: 'center',
            marginBottom: theme.spacing(1)
        }
    },
    titleRight: {
        [theme.breakpoints.up('sm')]: {
            textAlign: 'right'
        },
        [theme.breakpoints.down('xs')]: {
            textAlign: 'center'
        }
    },
    divider: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    leftSide: {
        paddingRight: theme.spacing(1),
        [theme.breakpoints.up('sm')]: {
            textAlign: 'left'
        },
        [theme.breakpoints.down('xs')]: {
            textAlign: 'right'
        }
    },
    rightSide: {
        paddingRight: theme.spacing(1),
        [theme.breakpoints.up('sm')]: {
            textAlign: 'left'
        },
        [theme.breakpoints.down('xs')]: {
            textAlign: 'center'
        }
    }
}));

const InfoCard = ({ getCurrentProfile, auth, profile }) => {
    useEffect(() => {
        getCurrentProfile();
    }, []);

    const classes = useStyles();

    const info = {
        name: 'andrew',
        age: 28
    };

    const title = 'Account';

    return (
        <div>
            {/* Title Grid */}
            <Grid container className={classes.titleGrid}>
                <Grid item xs={12} sm={6} className={classes.titleLeft}>
                    <Typography variant="h4">My Profile</Typography>
                </Grid>
                <Grid item xs={12} sm={6} className={classes.titleRight}>
                    <Button variant="contained" color="secondary">
                        Change Password
                    </Button>
                </Grid>
            </Grid>

            {/* MyProfile Cards */}
            <Paper className={classes.card}>
                <Grid container spacing={3} spacing={0}>
                    <Grid item xs={12} sm={5} className={classes.leftSide}>
                        <h2 className={classes.header}>Account</h2>
                        <Typography color="primary" className={classes.header}>
                            <Link>Edit</Link>
                        </Typography>
                        <Hidden smUp>
                            <Divider
                                variant="middle"
                                className={classes.divider}
                            ></Divider>
                        </Hidden>
                    </Grid>
                    <Grid item xs={12} sm={7} className={classes.rightSide}>
                        <Typography>
                            <b>Profile</b>
                        </Typography>
                        <Typography>Profile mine</Typography>
                        <Typography>
                            <b>Name</b>
                        </Typography>
                        <Typography>asdnd asdndd</Typography>
                    </Grid>
                </Grid>
            </Paper>

            <Paper className={classes.card}>
                <Grid container spacing={3} spacing={0}>
                    <Grid item xs={12} sm={5} className={classes.leftSide}>
                        <h2 className={classes.header}>Account</h2>
                        <Typography color="primary" className={classes.header}>
                            <Link>Edit</Link>
                        </Typography>
                        <Hidden smUp>
                            <Divider
                                variant="middle"
                                className={classes.divider}
                            ></Divider>
                        </Hidden>
                    </Grid>
                    <Grid item xs={12} sm={7} className={classes.rightSide}>
                        <Typography>
                            <b>Profile</b>
                        </Typography>
                        <Typography>Profile mine</Typography>
                        <Typography>
                            <b>Name</b>
                        </Typography>
                        <Typography>asdnd asdndd</Typography>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
};

InfoCard.propTypes = {
    getCurrentProfile: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    profile: state.profile
});

export default connect(mapStateToProps, { getCurrentProfile })(InfoCard);
