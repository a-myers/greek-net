import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logout } from '../../actions/auth';
import { Redirect } from 'react-router-dom';

//material-ui
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Menu, MenuItem } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex'
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            display: 'none'
        }
    },
    appBarDrawerMargin: {
        // [theme.breakpoints.up('sm')]: {
        //     // width: `calc(100% - ${drawerWidth}px)`,
        //     // marginLeft: drawerWidth,
        // },

        zIndex: theme.zIndex.drawer + 1
    },
    drawer: {
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            flexShrink: 0
        }
    },
    drawerPaper: {
        width: drawerWidth
    },
    title: {
        flexGrow: 1
    },
    buttonMargin: {
        margin: '5px'
    },
    toolbar: theme.mixins.toolbar
}));

const Navbar = ({ auth: { isAuthenticated, loading }, logout }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const authLinks = (
        <div>
            <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                <AccountCircle />
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                open={open}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>My Account</MenuItem>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </Menu>
        </div>
    );

    const guestLinks = (
        <div>
            <Button
                variant="contained"
                m="10px"
                className={classes.buttonMargin}
                href="/login"
            >
                Login
            </Button>
            <Button
                variant="contained"
                color="secondary"
                m="2px"
                href="/register"
            >
                Register
            </Button>
        </div>
    );

    const authNav = (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.appBarDrawerMargin}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h5" className={classes.title}>
                        <Link
                            href="/dashboard"
                            color="inherit"
                            underline="none"
                        >
                            Greek-Net
                        </Link>
                    </Typography>
                    {!loading && <Fragment>{authLinks}</Fragment>}
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer}>
                <Hidden mdUp implementation="css">
                    <SwipeableDrawer
                        variant="temporary"
                        anchor={'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        onOpen={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper
                        }}
                        ModalProps={{
                            keepMounted: true
                        }}
                    >
                        test1
                    </SwipeableDrawer>
                </Hidden>
                <Hidden smDown implementation="css">
                    <Drawer
                        variant="permanent"
                        classes={{
                            paper: classes.drawerPaper
                        }}
                        open
                    >
                        <div className={classes.toolbar}></div>
                        test
                    </Drawer>
                </Hidden>
            </nav>
            <div className={classes.toolbar} />
        </div>
    );

    const guestNav = (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h5" className={classes.title}>
                        <Link
                            href="/dashboard"
                            color="inherit"
                            underline="none"
                        >
                            Greek-Net
                        </Link>
                    </Typography>
                    {!loading && <Fragment>{guestLinks}</Fragment>}
                </Toolbar>
            </AppBar>
        </div>
    );

    return (
        <div>
            <Fragment>{isAuthenticated ? authNav : guestNav}</Fragment>
        </div>
    );
};

Navbar.propTypes = {
    logout: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, { logout })(Navbar);
