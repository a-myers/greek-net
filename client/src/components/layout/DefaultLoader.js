import React from 'react'
import Loader from 'react-loader-spinner'

const DefaultLoader = () => {
    

    return(
        <div
            style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)'
            }}
            >
            <Loader
            type="Oval"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={3000} //3 secs
    
            />
        </div>
        
       );
};
export default DefaultLoader;